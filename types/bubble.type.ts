export interface Bubble {
  id: number,
  name: string,
  image: string
}
