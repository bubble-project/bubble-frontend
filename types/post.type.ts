export interface PostType {
  id: number
  title: string,
  image?: string,
  headline: string
}
